from django.db import models


class Entry(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    publish_date = models.DateTimeField()
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    content = models.TextField()

    class Meta:
        ordering = ('publish_date', 'created',)
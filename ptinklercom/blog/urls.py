from django.conf.urls import patterns, url

from .views import EntryDetail, EntryList

urlpatterns = patterns(
    '',
    url(r'^entries/$', EntryList.as_view()),
    url(r'^entries/(?P<pk>[0-9]+)/$', EntryDetail.as_view()),
)